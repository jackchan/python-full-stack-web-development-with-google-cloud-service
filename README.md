# Python Full Stack Web Development with Google Cloud Service - Using Python Flask framework with MongoDB, Google Cloud Storage, Google Cloud API to build a social web application

You look at social platforms like facebook, twitter, have you ever imagined yourself being able to create one for yourself? Social web application is getting very popular, but to your surprise, it is not hard to build one. Have you ever imagined telling your friends that you can build a full stack web application from scratch all by yourself? You can learn it in this course!

Programming is one of the very important skills that people value a lot in this era. Not only it can help you get a well-paid software engineer job, but it can also train your brain to think in a logically manner. We are sure you agree with that but you are not sure where and how to start. You may already know some basics about programming, which is good, but you may think that is another level to build a useful application by yourself. Don't worry, in this course, we will guide you how to create your first python full stack web application step by step. You may become the next Jeff Bazos or Mark Zuckerburg!

## What exactly will you learn?

This course has a goal, which is to show you how you can build real life full stack web application using python. 

Unlike other programming courses which spent a lot of time on covering the syntax and logic, this course focus on the practical usage of the language. We assume you have basic understanding about the programming language (Python), we then teach you how you can build this social party web application from scratch. 

This course also cover a lot of latest technology like Mongo DB, Google cloud storage, Google Location API etc. Since we are showing you how to build the online web application step by step, you will see us using the free online IDE CodeAnywhere. Your application is runnable in the free server by provided by CodeAnywhere. 

You can also take this chance to revise important programming concept in Python. 

## Features of the party go application that you will write

* Online web application with very nice layout
* Allow users to create their own accounts and edit user profiles
* Users are allowed to upload profiles pictures, party pictures
* Create parties with customized information like location, date etc. 
* Data are stored in online document based database (Mongo DB)
* User profile pictures, party pictures are stored using Google cloud storage

Apart from the above, we incorporate the latest programming concept in the course as well. I hope with this little challenges, you can learn better. And if you come across any questions during the course, feel free to raise it. We are here to make sure you enjoy your learning process.

## What is Python?

Python is a simple, intuitive, yet powerful programming language. It is an high-level programming language invented back in 1991. It is a go-to language for beginners because it is so easy and expressive. You can often express concepts with very few lines of code. That's why we choose Python as the programming language for this course!

However, don't have a misunderstanding that Python is only for beginner. It is used in many areas including data analysis, finance, scientific research, artificial intelligence etc. Python is a very powerful programming language and is well supported the community. There are lots of useful packages like Numpy, Scipy etc. It is a programming language that supports multiple programming paradigms like procedural, functional, object-oriented etc. 

In a nutshell, if you can only learn one programming language, Python is your choice! And if you are a beginner and you want to grab the basics before taking this course, we recommend you to enroll our python beginner course - Learn Python in a Day. You can find the course under our profile. 

## What is Flask?

Flask is a micro web framework written in Python. It is classified as a microframework because it does not require particular tools or libraries. It supports extensions that can add application features and making the web applications built on Flask more powerful. Extensions are updated far more regularly than the core Flask program. In this course, we will cover Flask as well as other necessary extensions that are required to build this production grade party web platform. Flask is commonly used with MongoDB (which we will cover in this course as well), allowing it more control over databases and history.

## Who are the instructors?

Jack comes from a software engineering background. He has built sophisticated systems for top tier investment banks. You can understand more about the instructor in their Udemy profile. 

Instructor: Jack Chan  
Course Designer: Raymond Chung

## Money back guaranteed!

We are so confident about our course. We are so sure that you will enjoy the course as well. Therefore we provide money back guaranteed. After you have enrolled the course and if you don't like it, you can have your money back! It is unconditional, Udemy backed, 30 days money-back guarantee so you can try this course risk free!

Sign up for our course today, and let's create this fun party web application together!